# OpenOCD

OpenOCD dient der Verbindung vom PC zum integrierten Emulator/Debugger des verwendeten Eval-Boards.

## Installation

### Windows

Entpacken Sie das [Zip-Archiv](http://www.freddiechopin.info/en/download/category/4-openocd) in einen Ordner Ihrer Wahl z.B **"c:\Programme\OpenOCD-0.8.0"**
Fügen Sie den Pfad **"C:\Programme\OpenOCD-0.8.0\bin"** zu Ihrem [Systempfad hinzu](http://www.java.com/de/download/help/path.xml).
Unter Windows heißt das Programm **openocd-0.8.0** und nicht **openocd**

### Mac OSX

Unter Mac OSX kann OpenOCD mittels [HomeBrew](http://brew.sh/) installiert werden.
**Achtung es wird Version 0.8.x benötigt**

Damit HomeBrew installiert werden kann wird Ruby benötigt.


### Test der Installation

Nachdem Sie OpenOCD zu Ihrem Pfad hinzugefügt haben öffnen Sie ein Terminal/Komando-Shell und geben das Kommando openocd ein. Sie sollten eine Meldung die in etwa wie folgt lautet sehen:

    Open On-Chip Debugger 0.8.0-dev-00379-g17fddb4 (2014-03-09-11:40)
    Licensed under GNU GPL v2
    For bug reports, read
    	http://openocd.sourceforge.net/doc/doxygen/bugs.html
    Runtime Error: embedded:startup.tcl:47: Can't find openocd.cfg
    in procedure 'script'
    at file "embedded:startup.tcl", line 47
    Error: Debug Adapter has to be specified, see "interface" command
    in procedure 'init'

Unter Windows heißt das Programm **openocd-0.8.0** und nicht **openocd** die Ausgabe sieht wie folgt aus:

    C:\Users\deegech>openocd-0.8.0
    Open On-Chip Debugger 0.8.0 (2014-04-28-08:39)
    Licensed under GNU GPL v2
    For bug reports, read
            http://openocd.sourceforge.net/doc/doxygen/bugs.html
    Runtime Error: embedded:startup.tcl:47: Can't find openocd.cfg
    in procedure 'script'
    at file "embedded:startup.tcl", line 47
    Error: Debug Adapter has to be specified, see "interface" command
    in procedure 'init'


Erhalten Sie eine Fehlermeldung das das Programm nicht gefunden werden kann hat das Hinzufügen zum Systempfad nicht geklappt.

## Inbetriebnahme 32F072BDISCOVERY

Für eine Verbindung zwischen OpenOCD und unserem Discovery Borad benötigen wir eine Konfigurationsdatei. Diese befindet sich unterhalb des OpenOCD Installationsordners.

### Windows

Falls Sie unter Windows OpenOCD in das Verzeichnis C:\Programme installiert haben finden Sie die Konfigurationsdateien unter diesem Pfad:

    C:\Programme\scripts\board\stm32f0discovery.cfg

### Linux

Falls Sie OpenOCD unter /usr/local installiert haben finden Sie die Konfigurationsdateien hier:

    /usr/local/share/openocd/scripts/board/stm32f0discovery.cfg

### Mac OS X

Unter MacOS kann man mit folgendem Kommando auf dem Terminal herausfinden in welchen Ordner HomeBrew die Pakete installiert

    $ echo `brew --prefix`/bin

Anschließend liefert das Kommando **find** Hinweise wo sich die Konfiguration befindet

    find <HomeBrewPfad> -name "*stm32f0discovery.cfg*"

Alternativ kann eine Datei mit folgendem Inhalt angelegt werden:

    source [find interface/stlink-v2.cfg]
    set WORKAREASIZE 0x2000
    source [find target/stm32f0x_stlink.cfg]
    # use hardware reset, connect under reset
    reset_config srst_only srst_nogate

### Der erste Kontakt

Nun kann OpenOCD mit der entsprechenden Konfiguration gestartet werden

    openocd -f /usr/local/share/openocd/scripts/board/stm32f0discovery.cfg

oder

    openocd-0.8.0 -f C:\Programme\scripts\board\stm32f0discovery.cfg

Das Ergebnis sollte dann wie folgt aussehen:

    $ openocd -f /usr/local/share/openocd/scripts/board/stm32f0discovery.cfg
    Open On-Chip Debugger 0.8.0-dev-00350-g6c74255 (2014-02-21-21:00)
    Licensed under GNU GPL v2
    For bug reports, read
        http://openocd.sourceforge.net/doc/doxygen/bugs.html
    srst_only separate srst_nogate srst_open_drain connect_deassert_srst
    Info : This adapter doesn't support configurable speed
    Info : STLINK v2 JTAG v17 API v2 SWIM v0 VID 0x0483 PID 0x3748
    Info : using stlink api v2
    Info : Target voltage: 2.886792
    Info : stm32f0x.cpu: hardware has 4 breakpoints, 2 watchpoints

Standardmäßig öffnet OpenOCD zwei TCP/IP Ports mit welchen mit OpenOCD kommuniziert werden kann.

1. Der Telnet Port (**4444**)
2. Der GDB Remote Debugging port (**3333**)

Zum Test ob alles richtig funktioniert bauen wir eine Verbindung zum Telnet Port auf. Telnet sollte unter MacOS X und verfügbar sein. Unter Windows ab 7 muss [Putty](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html)
oder [TeraTerm](http://www.heise.de/download/teraterm-pro.html) verwendet verden

    telnet localhost 4444

Die Ausgabe sieht wie folgt aus:

    $ telnet localhost 4444
    Trying 127.0.0.1...
    Connected to localhost.
    Escape character is '^]'.
    Open On-Chip Debugger
    >

Folgende mit dem Kommando **help** können Sie sich eine Hilfe ausgeben lassen. Das Kommando **reset halt** setzt die CPU zurück und hält diese an.

    > reset halt
    target state: halted
    target halted due to debug-request, current mode: Thread
    xPSR: 0xc1000000 pc: 0x08008820 msp: 0x20004000

Eine Liste mit den verfügbaren Kommandos findet man im [OpenOCD Online Manual](http://openocd.sourceforge.net/doc/html/General-Commands.html#General-Commands)