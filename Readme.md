# Information

Dieses Git Repository dient zur Dokumentation des Kurses Mikrocomputertechnik 3 für den Kurs TEA12 an der Dualen Hochschule Baden Württemberg Ravensburg Campus Friedrichshafen.

# Ziel
Während der Vorlesung Mikrocomputertechnik 3 soll mit Hilfe des [32F072BDISCOVERY](http://www.st.com/web/catalog/tools/FM116/SC959/SS1532/LN1848/PF259724?s_searchtype=partnumber) 
eine [Computer-Maus](http://de.wikipedia.org/wiki/Maus_%28Computer%29) entwickelt werden. Diese wird mittels USB mit dem Host-PC verbunden. Die "Maus" kommuniziert über das 
[USB-HID](http://en.wikipedia.org/wiki/Human_interface_device) Protokoll. Die Bewegung erfasst der [L3GD20, ST MEMS motion sensor](http://www.st.com/web/catalog/sense_power/FM89/SC1288/PF252443?sc=internet/analog/product/252443.jsp)

## Fehler bei der Installation

Sollten Sie bei der Installation der beschriebenen Software auf Probleme stoßen schreiben Sie einfach einen [Issue](https://bitbucket.org/dhbw-fn-micro/tea12/issues?status=new&status=open)

Dabei sollten Sie die folgenden Richtlinien beachten:

### Grundsätze

Genau beschriebene Fehlermeldungen werden schneller behoben. Dieser Leitfaden erklärt, wie Sie solche Berichte schreiben sollten.

- Seien Sie präzise
- Seien Sie klar - erklären Sie so, dass die anderen Ihren Bug nachvollziehen können
- Beschreiben Sie nur einen Bug pro Meldung
- Kein Bug ist zu banal, um gemeldet zu werden - kleine Bugs könnten größere Fehler verstecken
- Trennen Sie Fakten klar von Vermutungen ab
- Schreiben Sie die Bug-Meldung - wenn möglich - auf Englisch
- Verwenden Sie wenn möglich Bilder um Ihr Problem zu beschreiben

Diese Hinweise wurden den [Richtlinien für das Mozilla Projekt (Firefox)](https://developer.mozilla.org/de/docs/Richtlinien_zum_Schreiben_eines_Bugreports) entnommen.

